﻿Imports Connect_Four.Data.Connect_Four.Data

Namespace Connect_Four.Domain
    ''' <summary>
    ''' DTO class for transmitting and storing the current game state
    ''' </summary>
    Public Class State
        Public Property IsFinished As Boolean

        Public Property Board As Board

        Public Property Player As String

        Public Property Message As String
    End Class
End Namespace
