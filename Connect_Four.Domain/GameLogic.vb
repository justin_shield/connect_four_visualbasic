﻿
Imports System.ComponentModel
Imports System.Text
Imports Connect_Four.Data.Connect_Four.Data

Namespace Connect_Four.Domain
    Public Class ConnectFourLogic
        Implements IGameLogic
        Private Const WinCondition As String = "tttt"

        Public Function CheckForDraw(board As Board) As Boolean Implements IGameLogic.CheckForDraw
            Return Not CheckForWin(board, Checker.Yellow) AndAlso Not CheckForWin(board, Checker.Red) AndAlso BoardFull(board)
        End Function

        ''' <summary>
        ''' Returns true if the selected colour checker wins 4 in a row
        ''' </summary>
        Public Function CheckForWin(board As Board, checker As Checker) As Boolean Implements IGameLogic.CheckForWin
            If board Is Nothing Then
                Throw New ArgumentNullException("board", "cannot be null")
            End If

            If checker = Checker.Empty Then
                Throw New InvalidEnumArgumentException("Checker cannot be empty")
            End If

            ' Check for an invalid board
            If board.Rows <= 0 OrElse board.Columns <= 0 Then
                Return False
            End If

            If CheckRows(board, checker) Then
                Return True
            End If

            If CheckColumns(board, checker) Then
                Return True
            End If

            If CheckDiagonals(board, checker) Then
                Return True
            End If

            Return False
        End Function

        ''' <summary>
        ''' Returns true if there are no empty places left on the board
        ''' </summary>        
        Public Function BoardFull(board As Board) As Boolean
            For row As Integer = 0 To board.Rows - 1
                For col As Integer = 0 To board.Columns - 1
                    If CheckLocation(board, row, col, Checker.Empty) Then
                        Return False
                    End If
                Next
            Next

            Return True
        End Function

        ' Returns true of the selected checker is in that board location
        ' Returns false if the position is out of bounds
        Private Shared Function CheckLocation(board As Board, row As Integer, column As Integer, checker As Checker) As Boolean
            If row < 0 OrElse row >= board.Rows OrElse column < 0 OrElse column >= board.Columns Then
                Return False
            End If

            Return board.Matrix(row, column) = checker
        End Function

        ' Returns true if the selected checker is 4 in a row
        Private Function CheckRows(board As Board, checker As Checker) As Boolean
            Dim rowsString = New StringBuilder()
            For row As Integer = 0 To board.Rows - 1
                rowsString.Clear()
                For col As Integer = 0 To board.Columns - 1
                    rowsString.Append(If(CheckLocation(board, row, col, checker), "t", "f"))
                Next

                If rowsString.ToString().Contains(WinCondition) Then
                    Return True
                End If
            Next

            Return False
        End Function

        ' Returns true if the selected checker is 4 in a column
        Private Shared Function CheckColumns(board As Board, checker As Checker) As Boolean
            Dim colString = New StringBuilder()

            For col As Integer = 0 To board.Columns - 1
                colString.Clear()

                For row As Integer = 0 To board.Rows - 1
                    colString.Append(If(CheckLocation(board, row, col, checker), "t", "f"))
                Next

                If colString.ToString().Contains(WinCondition) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ' Returns true if the selected checker is 4 in a diagonal either left or right
        Private Shared Function CheckDiagonals(board As Board, checker As Checker) As Boolean
            Dim diagsRight = New StringBuilder()
            Dim diagsLeft = New StringBuilder()

            For row As Integer = 0 To board.Rows - 1
                For column As Integer = 0 To board.Columns - 1
                    diagsRight.Clear()
                    diagsLeft.Clear()
                    For i As Integer = 0 To 3
                        diagsRight.Append(If(CheckLocation(board, row + i, column + i, checker), "t", "f"))
                        diagsLeft.Append(If(CheckLocation(board, row + i, column - i, checker), "t", "f"))
                    Next

                    If diagsRight.ToString().Contains(WinCondition) OrElse diagsLeft.ToString().Contains(WinCondition) Then
                        Return True
                    End If
                Next
            Next
            Return False
        End Function


    End Class
End Namespace


