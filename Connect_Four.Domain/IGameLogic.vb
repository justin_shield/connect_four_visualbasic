﻿Imports Connect_Four.Data.Connect_Four.Data

Namespace Connect_Four.Domain
    Public Interface IGameLogic
        Function CheckForDraw(board As Board) As Boolean
        Function CheckForWin(board As Board, checker As Checker) As Boolean
    End Interface
End Namespace
