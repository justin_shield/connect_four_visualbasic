﻿Imports Connect_Four.Data.Connect_Four.Data

Namespace Connect_Four.Domain


    ''' <summary>
    ''' Class contains the current game state
    ''' </summary>
    Public Class Game
        Private ReadOnly _logic As IGameLogic

        Private _playerTurn As Integer
        Private _board As Board
        Private _players As List(Of Player)
        Private _currentPlayer As Player

        Private _isFinished As Boolean = True

        Public Sub New(logic As IGameLogic)
            _logic = logic
        End Sub

        Public Function Start(rows As Integer, columns As Integer) As State
            _players = New List(Of Player)() From {
                New Player(Checker.Yellow),
                New Player(Checker.Red)
            }

            _board = New Board(rows, columns)
            _playerTurn = -1
            _isFinished = False

            Return NewTurn()
        End Function

        Private Function NewTurn() As State
            ' Set the player turn
            _playerTurn += 1
            If _playerTurn >= _players.Count Then
                _playerTurn = 0
            End If

            ' Set the Current Player
            _currentPlayer = _players(_playerTurn)

            Return New State() With {
                .Board = _board,
                .Player = _currentPlayer.Colour.ToString(),
                .Message = String.Empty,
                .IsFinished = _isFinished
            }
        End Function

        Public Function PlaceChecker(column As Integer) As State
            Try
                If _board.PlaceChecker(_currentPlayer.Colour, column) < 0 Then
                    Throw New ArgumentOutOfRangeException("Invalid column number")
                End If
            Catch ex As Exception
                Return CreateState(ex.Message)
            End Try

            If _logic.CheckForWin(_board, _currentPlayer.Colour) Then
                _isFinished = True
                Return CreateState(String.Format("{0} WINS!", _currentPlayer.Colour))
            End If

            If _logic.CheckForDraw(_board) Then
                _isFinished = True
                Return CreateState("Draw!")
            End If

            Return NewTurn()
        End Function


        Private Function CreateState(Optional message As String = "") As State
            Return New State() With {
                .Board = _board,
                .Message = message,
                .IsFinished = _isFinished,
                .Player = _currentPlayer.Colour.ToString()
            }
        End Function


    End Class
End Namespace


