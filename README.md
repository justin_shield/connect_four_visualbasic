# Connect Four
Connect four is a windows console application that plays a game of connect four against yourself.

### Running Connect Four

To run connect four, open a command prompt.  
Navigate to your installed directory and type.

```sh
Connect_Four.exe
```

### Requirements
Connect four requires that you have the .NET Framework 4.6 installed.  If you don't already have it you can download it from the following location.

[Download .NET Framework 4.6](https://www.microsoft.com/en-au/download/details.aspx?id=48130)

### How to play
If you are unfamilar with the rules of playing connect four you can find a copy of them here:
[http://www.andersbaumann.dk/connectfour/rulesConnectFour.html](http://www.andersbaumann.dk/connectfour/rulesConnectFour.html)

You play the game by first entering the board dimensions (number of rows, number of columns) separated by a space.
```sh
> Please enter the board dimensions (number of rows, number of columns)
> 5 5 
```
The game will display the initial game board.
```sh
o o o o o
o o o o o
o o o o o
o o o o o
o o o o o
```

o = empty space
y = yellow checker
r = red checker

Folowed by the players turn
```sh
> Yellows turn:
> 1
```
You enter a column number from 1 - n dimensions you entered at the beginning of the name
It will then display the current state of the board with your newly enterd checker.
```sh
o o o o o
o o o o o
o o o o o
o o o o o
y o o o o
> Reds turn:
```
You then enter the column number you wish to place your coloured checker.

The game continues until either player wins or draws.

If you wish to play again, type either y or yes
```sh
> Do you wish to play again? Y/N?
> n
> Thanks for playing, press any key to exit
```
### Who do I talk to? ###

* Any questions or queries, please contact [Justin Shield](justin.n.shield@gmail.com)