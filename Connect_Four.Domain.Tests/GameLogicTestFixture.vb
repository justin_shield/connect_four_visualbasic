﻿
Imports System.ComponentModel
Imports Connect_Four.Data.Connect_Four.Data
Imports Connect_Four.Domain.Connect_Four.Domain
Imports NUnit.Framework

Namespace Connect_Four.Domain.Tests
    <TestFixture>
    Public Class GameLogicTestFixture
        Private _board As Board
        Private _logic As ConnectFourLogic

        <SetUp>
        Public Sub Setup()
            _board = New Board(4, 4)
            _logic = New ConnectFourLogic()
        End Sub

        <TearDown>
        Public Sub TearDown()
        End Sub

        <Test>
        Public Sub CanCreateGameLogic()
            ' Arrange            

            ' Act            

            ' Assert
            Assert.IsNotNull(_logic)
        End Sub

        <Test>
        Public Sub CheckForWin_NoBoard_ThrowsException()
            ' Arrange            

            ' Act            

            ' Assert
            Assert.Throws(Of ArgumentNullException)(Function() _logic.CheckForWin(Nothing, Checker.Empty))
        End Sub

        <Test>
        Public Sub CheckForWin_EmptyChecker_ThrowsException()
            ' Arrange            

            ' Act            

            ' Assert
            Assert.Throws(Of InvalidEnumArgumentException)(Function() _logic.CheckForWin(_board, Checker.Empty))
        End Sub

        <TestCase(Checker.Yellow)>
        <TestCase(Checker.Red)>
        Public Sub CheckForWin_Horizontal_ReturnsTrue(checker__1 As Checker)
            ' Arrange                        
            _board.Matrix(0, 0) = checker__1
            _board.Matrix(0, 1) = checker__1
            _board.Matrix(0, 2) = checker__1
            _board.Matrix(0, 3) = checker__1

            ' Act

            ' Assert
            CheckForWin(checker__1, _board)
        End Sub



        <TestCase(Checker.Yellow)>
        <TestCase(Checker.Red)>
        Public Sub CheckForWin_Vertical_ReturnsTrue(checker__1 As Checker)
            ' Arrange
            _board.Matrix(0, 0) = checker__1
            _board.Matrix(1, 0) = checker__1
            _board.Matrix(2, 0) = checker__1
            _board.Matrix(3, 0) = checker__1

            ' Act

            ' Assert
            CheckForWin(checker__1, _board)
        End Sub


        <TestCase(Checker.Yellow)>
        <TestCase(Checker.Red)>
        Public Sub CheckForWin_RightDiagonal_ReturnsTrue(checker__1 As Checker)
            ' Arrange
            _board.Matrix(0, 0) = checker__1
            _board.Matrix(1, 1) = checker__1
            _board.Matrix(2, 2) = checker__1
            _board.Matrix(3, 3) = checker__1

            ' Act

            ' Assert
            CheckForWin(checker__1, _board)
        End Sub

        <TestCase(Checker.Yellow)>
        <TestCase(Checker.Red)>
        Public Sub CheckForWin_LeftDiagonal_ReturnsTrue(checker__1 As Checker)
            ' Arrange
            _board.Matrix(3, 0) = checker__1
            _board.Matrix(2, 1) = checker__1
            _board.Matrix(1, 2) = checker__1
            _board.Matrix(0, 3) = checker__1

            ' Act

            ' Assert
            CheckForWin(checker__1, _board)
        End Sub

        <Test>
        Public Sub CheckForFull_Returns_False()
            ' Arrange

            ' Act
            Dim isFull = _logic.BoardFull(_board)

            ' Assert
            Assert.IsFalse(isFull, "Board should not be full")
        End Sub

        <TestCase(Checker.Yellow)>
        <TestCase(Checker.Red)>
        Public Sub CheckForFull_Returns_True(checker__1 As Checker)
            ' Arrange
            For row As Integer = 0 To _board.Rows - 1
                For col As Integer = 0 To _board.Columns - 1
                    _board.Matrix(row, col) = checker__1
                Next
            Next

            ' Act
            Dim isFull = _logic.BoardFull(_board)

            ' Assert
            Assert.IsTrue(isFull, "Board should be full")
        End Sub

#Region "Helpers"

        Private Shared Function SwitchChecker(checker__1 As Checker) As Checker
            Return If(checker__1 = Checker.Yellow, Checker.Red, Checker.Yellow)
        End Function

        Private Sub CheckForWin(checker As Checker, board As Board)
            Dim otherChecker = SwitchChecker(checker)

            ' Act
            Dim winCondition = _logic.CheckForWin(board, checker)
            Dim loseCondition = _logic.CheckForWin(board, otherChecker)

            ' Assert
            Assert.IsTrue(winCondition, "{0} should win", checker)
            Assert.IsFalse(loseCondition, "{0} should lose", otherChecker)
        End Sub

#End Region



    End Class
End Namespace
