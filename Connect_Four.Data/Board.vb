﻿
Imports System.ComponentModel

Namespace Connect_Four.Data

    Public Class Board
        Private ReadOnly _matrix As Checker(,)
        Private ReadOnly _rows As Integer
        Private ReadOnly _columns As Integer

        Private _lastPlaced As Checker

        Public ReadOnly Property Rows() As Integer
            Get
                Return _rows
            End Get
        End Property
        Public ReadOnly Property Columns() As Integer
            Get
                Return _columns
            End Get
        End Property
        Public ReadOnly Property Matrix() As Checker(,)
            Get
                Return _matrix
            End Get
        End Property

        Public Sub New(rows As Integer, columns As Integer)
            If rows < 4 Then
                Throw New ArgumentOutOfRangeException("rows", "must be larger than 3")
            End If

            If columns < 4 Then
                Throw New ArgumentOutOfRangeException("columns", "must be larger than 3")
            End If

            _rows = rows
            _columns = columns
            _matrix = New Checker(_rows - 1, _columns - 1) {}

            InitializeBoard()
        End Sub

        Private Sub InitializeBoard()
            For rowNum As Integer = 0 To _rows - 1
                For colNum As Integer = 0 To _columns - 1
                    _matrix(rowNum, colNum) = Checker.Empty
                Next
            Next
        End Sub

        ''' <summary>
        ''' Returns the row the checker has been placed into for that columns
        ''' </summary>        
        ''' <returns>Negative 1 if its unable to place a checker in that column</returns>
        Public Function PlaceChecker(checker__1 As Checker, column As Integer) As Integer
            If checker__1 = Checker.Empty Then
                Throw New InvalidEnumArgumentException("Cannot place an empty checker")
            End If

            ' If the last placed checker is the same, then this is an invalid move
            ' The column is less than 0 
            If _lastPlaced = checker__1 OrElse column < 0 OrElse column >= _columns Then
                ' The column is greater than the num of columns
                Return -1
            End If

            ' Cycle through the rows on that column
            For row As Integer = 0 To _rows - 1
                If _matrix(row, column) <> Checker.Empty Then
                    ' Continue through the rows until we find an empty place
                    Continue For
                End If

                _matrix(row, column) = checker__1
                _lastPlaced = checker__1
                Return row
            Next

            ' Cannot place another checker in that column
            Return -1
        End Function

    End Class
End Namespace

