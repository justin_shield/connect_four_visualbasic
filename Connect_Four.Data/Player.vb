﻿
Namespace Connect_Four.Data
    Public Class Player
        Private ReadOnly _colour As Checker
        Private _moveNumber As Integer

        Public ReadOnly Property Colour() As Checker
            Get
                Return _colour
            End Get
        End Property

        Public Sub New(colour As Checker)
            _colour = colour
        End Sub

    End Class
End Namespace
