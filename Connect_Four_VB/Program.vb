﻿
Imports System.Text
Imports Connect_Four.Data.Connect_Four.Data
Imports Connect_Four.Domain.Connect_Four.Domain

Module Program
    Private _connectFour As Game
    Private _gameState As State

    Sub Main(args As String())
        While True
            NewGame()

            If Not PlayAgain() Then
                Exit While
            End If
        End While

        Console.WriteLine("Thanks for playing, press any key to exit")
        Console.ReadKey()
    End Sub


    Private Sub NewGame()
        Console.WriteLine("Please enter the board dimensions (number of rows, number of columns)")
        Dim dimensions = Console.ReadLine().Split(" "c)

        Try
            Dim rows = Convert.ToInt32(dimensions(0))
            Dim cols = Convert.ToInt32(dimensions(1))

            _connectFour = New Game(New ConnectFourLogic())
            _gameState = _connectFour.Start(rows, cols)
        Catch oor As ArgumentOutOfRangeException
            Console.WriteLine(oor.Message)
            NewGame()
        Catch generatedExceptionName As Exception
            Console.WriteLine("Unable to parse one or more parameters")
            NewGame()
        End Try

        While Not _gameState.IsFinished
            DisplayBoard(_gameState)
            DisplayTurn(_gameState)

            Dim col As Integer = 0

            Try
                col = Convert.ToInt32(Console.ReadLine())
            Catch generatedExceptionName As Exception
                Console.WriteLine("Unable to parse column")
                Continue While
            End Try

            Dim state = _connectFour.PlaceChecker(col - 1)

            If state.IsFinished Then
                DisplayBoard(state)
            End If

            _gameState = state

            Console.WriteLine(_gameState.Message)
        End While


    End Sub

    Private Function PlayAgain() As Boolean
        Console.WriteLine("Game Over!")
        Console.WriteLine("Do you wish to play again? Y/N?")
        Return Console.ReadLine().StartsWith("y", StringComparison.OrdinalIgnoreCase)
    End Function

    Private Sub DisplayBoard(gameState As State)
        Dim board = gameState.Board
        Dim output = New StringBuilder()
        For row As Integer = board.Rows - 1 To 0 Step -1
            output.Clear()
            For col As Integer = 0 To board.Columns - 1
                Dim cell = board.Matrix(row, col)
                Dim cellChar = If(cell = Checker.Empty, "o", If(cell = Checker.Yellow, "y", "r"))

                output.Append(cellChar)
            Next

            Console.WriteLine(String.Join(vbTab & vbTab, output.ToString()))
        Next
    End Sub

    Private Sub DisplayTurn(gameState As State)
        Console.WriteLine("{0}s turn:", gameState.Player)
    End Sub

End Module

