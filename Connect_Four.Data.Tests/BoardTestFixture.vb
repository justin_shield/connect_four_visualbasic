﻿Imports Connect_Four.Data.Connect_Four.Data
Imports NUnit.Framework

Namespace Connect_Four.Data.Tests
    <TestFixture>
    Public Class BoardTestFixture
        Private _board As Board

        <SetUp>
        Public Sub Setup()
        End Sub

        <TearDown>
        Public Sub TearDown()
            _board = Nothing
        End Sub


        <TestCase(-1, 4)>
        <TestCase(0, 4)>
        <TestCase(1, 4)>
        <TestCase(2, 4)>
        <TestCase(3, 4)>
        <TestCase(4, -1)>
        <TestCase(4, 0)>
        <TestCase(4, 1)>
        <TestCase(4, 2)>
        <TestCase(4, 3)>
        <TestCase(0, 0)>
        <TestCase(1, 1)>
        <TestCase(2, 2)>
        <TestCase(3, 3)>
        Public Sub TestInvalidBoardDimensions(rows As Integer, columns As Integer)
            Assert.Throws(Of ArgumentOutOfRangeException)(Function() InlineAssignHelper(_board, New Board(rows, columns)))
        End Sub


        <TestCase(4, 4)>
        <TestCase(4, 5)>
        <TestCase(4, 6)>
        <TestCase(4, 7)>
        <TestCase(5, 4)>
        <TestCase(6, 4)>
        <TestCase(7, 4)>
        <TestCase(5, 5)>
        <TestCase(6, 6)>
        <TestCase(7, 7)>
        <TestCase(8, 8)>
        <TestCase(9, 9)>
        Public Sub TestValidBoardDimensions(rows As Integer, columns As Integer)
            ' Arrange

            ' Act
            _board = New Board(rows, columns)

            ' Assert
            Assert.That(Function() _board.Rows = rows)
            Assert.That(Function() _board.Columns = columns)

            For boardRow As Integer = 0 To rows - 1
                For boardCol As Integer = 0 To columns - 1
                    Assert.AreEqual(_board.Matrix(boardRow, boardCol), Checker.Empty)
                Next
            Next
        End Sub


        <TestCase(4, 4, 0)>
        <TestCase(4, 4, 1)>
        <TestCase(4, 4, 2)>
        <TestCase(4, 4, 3)>
        Public Sub TestValidCheckerPlacements(rows As Integer, columns As Integer, checkerColumn As Integer)
            ' Arrange
            _board = New Board(rows, columns)
            Dim checker__1 = Checker.Red

            ' Act

            ' Assert
            For row As Integer = 0 To rows - 1
                checker__1 = If(checker__1 = Checker.Red, Checker.Yellow, Checker.Red)
                ' Swap the checker colour for each row
                Assert.AreEqual(row, _board.PlaceChecker(checker__1, checkerColumn))
                Assert.AreEqual(checker__1, _board.Matrix(row, checkerColumn))
            Next

        End Sub

        <TestCase(4, 4, -1)>
        <TestCase(4, 4, 5)>
        <TestCase(4, 4, 6)>
        <TestCase(4, 4, 7)>
        Public Sub TestInvalidCheckerPlacements(rows As Integer, columns As Integer, checkerColumn As Integer)
            ' Arrange
            _board = New Board(rows, columns)
            Dim checker__1 = Checker.Red

            ' Act

            ' Assert
            For row As Integer = 0 To rows - 1
                checker__1 = If(checker__1 = Checker.Red, Checker.Yellow, Checker.Red)
                ' Swap the checker colour for each row
                ' placeChecker returns -1 for an invalid placement, otherwise it returns the row number
                Assert.AreEqual(-1, _board.PlaceChecker(checker__1, checkerColumn))
            Next
        End Sub

        <TestCase(4, 4, 0)>
        <TestCase(4, 4, 1)>
        <TestCase(4, 4, 2)>
        <TestCase(4, 4, 3)>
        Public Sub TestInvalidCheckerPlacementColour(rows As Integer, columns As Integer, checkerColumn As Integer)
            ' Arrange
            _board = New Board(rows, columns)

            ' Act

            ' Assert
            For row As Integer = 0 To rows - 1
                If row = 0 Then
                    Assert.AreEqual(0, _board.PlaceChecker(Checker.Red, checkerColumn))
                Else
                    ' The first one will work but the next colour needs to be Yellow               
                    Assert.AreEqual(-1, _board.PlaceChecker(Checker.Red, checkerColumn))

                End If
            Next
        End Sub

        Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function



    End Class
End Namespace


